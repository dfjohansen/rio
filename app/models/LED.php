<?php

class LED extends Eloquent {

	protected $table = 'l_artikel';
	
	protected $primaryKey = 'varenr';
	
	protected $connection = 'limestest';
	
	
	
	public static function beholdning($varenr){
		$sum = DB::connection('limestest')->table('l_locbeholdning')->select(DB::raw('sum(beholdning) as beholdning'))->where('varenr', '=', $varenr)->first();
		
		return $sum->beholdning;
		
	}
	
	
	public static function scopeLED($query){
		return $query->where('vtext', 'LIKE', '%LED%')->where('vgruppe', '=', '420090');
	}
	
	public static function fullVtext($varenr){
		$vare = DB::connection('limestest')->table('l_artikel')->where('varenr', '=', $varenr)->first();
		
		//dd($vare->vtext . $vare->vtext2 . $vare->vtext3 . $vare->text4);
		
		return $vare->vtext . ' ' . $vare->vtext2 . ' ' . $vare->vtext3 . ' ' . $vare->vtext4;
	}
	
	public static function getAllLED($offset, $number, $search, $sortBy, $sortOrder, $search
	, $varenr, $varetext, $watt, $lm, $mA, $K, $CRI, $aktiv) {
			
			
		$items = LED::LED()->select('*' ,
		DB::raw("
			SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4),
GREATEST(LOCATE(binary 'W ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)), LOCATE(binary 'W ', CONCAT(vtext, ' ', vtext2, ' ',
vtext3, ' ', vtext4)))-1), ' ', -1) as 'watt'"),
		DB::raw("SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ',
vtext2, ' ', vtext3, ' ', vtext4), GREATEST(LOCATE( binary 'lm ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
LOCATE(binary 'lm ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as 'lm'"),
		DB::raw("SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), GREATEST(LOCATE(BINARY 'mA',
CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)), LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ',
vtext4)))-1), ' ', -1) as 'mA'"), 
		DB::raw("SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4),
GREATEST(LOCATE(BINARY 'K ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)), LOCATE(BINARY 'K ', CONCAT(vtext, ' ',
vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as 'K'"),
		DB::raw("SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4),
GREATEST(LOCATE(BINARY 'CRI ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)), LOCATE(BINARY 'CRI ', CONCAT(vtext, ' ',
vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as 'CRI'"));



		if ($search) {
			$items->where(function($query) use($search) {
				$query->where('varenr', 'LIKE', '%' . $search)->orWhere('vtext', 'LIKE', '%' . $search . '%')
					->orWhere('vtext2', 'LIKE', '%' . $search . '%')
						->orWhere('vtext3', 'LIKE', '%' . $search . '%')
							->orWhere('vtext4', 'LIKE', '%' . $search . '%')
								->orWhere('vtext5', 'LIKE', '%' . $search . '%');
			});
		}

		
		if($varenr){
			$items->where('varenr', 'LIKE', '%' . $varenr);
		}
		
		if($varetext){
			$items->where(function($query) use($varetext) {
				$query->where('vtext' , 'LIKE', '%' . $varetext . '%')
					->orWhere('vtext2', 'LIKE', '%' . $varetext . '%')
					->orWhere('vtext3', 'LIKE', '%' . $varetext . '%')
					->orWhere('vtext4', 'LIKE', '%' . $varetext . '%');
			});
		}
		
		if($watt){
			$items->where(function($query) use($watt) {
				$query->where('vtext' , 'LIKE', '% ' . $watt . 'W%')
					->orWhere('vtext2', 'LIKE', '% ' . $watt . 'W%')
					->orWhere('vtext3', 'LIKE', '% ' . $watt . 'W%')
					->orWhere('vtext4', 'LIKE', '% ' . $watt . 'W%');
			});
		}
		
		if($lm){
			$items->where(function($query) use($lm) {
				$query->where('vtext' , 'LIKE',  '% ' . $lm . 'lm%')
					->orWhere('vtext2', 'LIKE', '% ' . $lm . 'lm%')
					->orWhere('vtext3', 'LIKE', '% ' . $lm . 'lm%')
					->orWhere('vtext4', 'LIKE', '% ' . $lm . 'lm%');
			});
		}
		
		
		if($mA){
			$items->where(function($query) use($mA) {
				$query->where('vtext' , 'LIKE',  '% ' . $mA . 'mA%')
					->orWhere('vtext2', 'LIKE', '% ' . $mA . 'mA%')
					->orWhere('vtext3', 'LIKE', '% ' . $mA . 'mA%')
					->orWhere('vtext4', 'LIKE', '% ' . $mA . 'mA%');
			});
		}
		
		if($K){
			$items->where(function($query) use($K) {
				$query->where('vtext' , 'LIKE',  '% ' . $K . 'K%')
					->orWhere('vtext2', 'LIKE', '% ' . $K . 'K%')
					->orWhere('vtext3', 'LIKE', '% ' . $K . 'K%')
					->orWhere('vtext4', 'LIKE', '% ' . $K . 'K%');
			});
		}
		
		if($CRI){
			$items->where(function($query) use($CRI) {
				$query->where('vtext' , 'LIKE',  '%' . $CRI . 'CRI%')
					->orWhere('vtext2', 'LIKE', '%' . $CRI . 'CRI%')
					->orWhere('vtext3', 'LIKE', '%' . $CRI . 'CRI%')
					->orWhere('vtext4', 'LIKE', '%' . $CRI . 'CRI%');
			});
		}
		
		if($aktiv){
			$items->where('aktiv', '=', $aktiv);
		}


		if($sortBy == "watt"){
			$items->orderBy(DB::raw("CAST(SUBSTRING_INDEX(
			LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
			GREATEST(LOCATE('W ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
			LOCATE('W ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) AS UNSIGNED)"), $sortOrder);
		} else if ($sortBy == "lm"){
			$items->orderBy(DB::raw("CAST(SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ',
vtext2, ' ', vtext3, ' ', vtext4), GREATEST(LOCATE('lm', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
LOCATE('lm', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as UNSIGNED)"), $sortOrder);
		} else if($sortBy == "mA") {
			$items->orderBy(DB::raw("SUBSTRING_INDEX(
			LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
			GREATEST(LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
			LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1)"), $sortOrder);
		} else if($sortBy == "K") {
			$items->orderBy(DB::raw("SUBSTRING_INDEX(
		LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
		GREATEST(LOCATE(BINARY 'K ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
		LOCATE(BINARY 'K ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1)"), $sortOrder);
		
		} else if($sortBy == "CRI") {
			$items->orderBy(DB::raw("SUBSTRING_INDEX( LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4),
GREATEST(LOCATE(BINARY 'CRI ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)), LOCATE(BINARY 'CRI ', CONCAT(vtext, ' ',
vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) "), $sortOrder);
		
		} else if ($sortBy) {
			$items = $items->orderBy($sortBy, $sortOrder);
		}

		$count = $items->count();

		$items->take($number)->skip($offset);


		//dd(Item::all()->count()->toSql());
		//dd($items->toSql());
		$instance = $items->get();
			
			
			
			
		return [
			'count' => $count,
			'items' => $instance,
			'totalCount' => LED::LED()->count(),
		];
	}

}
