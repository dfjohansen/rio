<?php

class Item extends Eloquent {

	protected $table = 'l_artikel';
	
	protected $primaryKey = 'varenr';
	
	protected $connection = 'limestest';
	
	
	public function varetext(){
		$varetext = DB::connection('limestest')->table('l_artikel')
			->select('vtext')->where('varenr', '=', $this->varenr)->get();
		
		return $varetext;
	}
	
	public function location(){
		$location = DB::connection('limestest')->table('l_locbeholdning')
			->select('location', 'beholdning')
				->where('varenr', '=', $this->varenr)
					->where('beholdning', '<>', 0)->get();
		
		return $location;
	}
	
	public static function itemInOrder(){
		$items = DB::connection('limestest')->table('l_ordlin')
			->join('l_artikel', 'l_ordlin.varenr', '=', 'l_artikel.varenr')
				->join('l_ordhvd', 'l_ordlin.ordrenr', '=', 'l_ordhvd.ordrenr')
					->join('e_krdopl', 'l_ordhvd.kontonr', '=', 'e_krdopl.levnr')
						->select('l_ordhvd.ordrenr', 'l_ordlin.opos', 'l_ordlin.varenr', 
		'l_artikel.vtext', 'l_ordlin.antal', 'l_ordlin.leveret', 
		'l_ordlin.termin', 'l_ordlin.levbekr', 'e_krdopl.levnr', 'e_krdopl.navn1')
			->where('l_ordhvd.otype', '=', 'k')
				->where('l_ordlin.otype', '=', 'k')
					->where('l_ordlin.olslut', '=', 1)
						->where('l_artikel.varenr', 'LIKE', '%ou')
							->where('e_krdopl.levnr', '>', '8732')
								->where('e_krdopl.levnr', '<>', '886292')
									->where('e_krdopl.levnr', '<>', '8757')
										->where('e_krdopl.levnr', '<>', '88738')->get();
		
		return [ 
			'items' => $items,
			'count' => count($items),
			'totalCount' => count($items)
		];
	}
	
	public static function beholdning($varenr){
		$sum = DB::connection('limestest')->table('l_locbeholdning')->select(DB::raw('sum(beholdning) as beholdning'))->where('varenr', '=', $varenr)->first();
		
		return $sum->beholdning;
		
	}
	
	
	public static function scopeDriver($query){
		return $query->where('vtext', 'LIKE', '%Driver%')->where('vgruppe', '=', '420000');
	}
	
	public static function fullVtext($varenr){
		$vare = DB::connection('limestest')->table('l_artikel')->where('varenr', '=', $varenr)->first();
		
		//dd($vare->vtext . $vare->vtext2 . $vare->vtext3 . $vare->text4);
		
		return $vare->vtext . ' ' . $vare->vtext2 . ' ' . $vare->vtext3 . ' ' . $vare->vtext4;
	}
	
	public static function getAllDriver($offset, $number, $search, $sortBy, $sortOrder, $search, $search0, $searchVtext, $watt, $mA, $volt, $aktiv) {
			
			
		$items = Item::Driver()->select('*' ,DB::raw("CAST(SUBSTRING_INDEX(
		LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
		GREATEST(LOCATE('W', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
		LOCATE('W', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as UNSIGNED) as 'watt'"),
		DB::raw("SUBSTRING_INDEX(
		LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
		GREATEST(LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
		LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as 'mA'"),
		DB::raw("SUBSTRING_INDEX(
		LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
		GREATEST(LOCATE(BINARY 'V', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
		LOCATE(BINARY 'V', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) as 'volt'"), 
		DB::raw('sum(l_locbeholdning.beholdning) as total'))->join('l_locbeholdning', 'l_artikel.varenr', '=', 'l_locbeholdning.varenr')->groupBy('l_locbeholdning.varenr')->join('l_artikel_t', 'l_artikel.varenr', '=', 'l_artikel_t.varenr');


		if ($search) {
			$items->where(function($query) use($search) {
				$query->where('varenr', 'LIKE', '%' . $search)->orWhere('vtext', 'LIKE', '%' . $search . '%')
					->orWhere('vtext2', 'LIKE', '%' . $search . '%')
						->orWhere('vtext3', 'LIKE', '%' . $search . '%')
							->orWhere('vtext4', 'LIKE', '%' . $search . '%')
								->orWhere('vtext5', 'LIKE', '%' . $search . '%');
			});
		}

		if($search0){
			$items->where('varenr', 'LIKE', '%' . $search0);
		}

		if($aktiv){
			$items->where('aktiv', '=', $aktiv);
		}

		if($watt){
			$items->where(function($query) use($watt) {
				$query->where('vtext', 'LIKE', '% ' . $watt . 'W%')
					->orWhere('vtext2', 'LIKE', '% ' . $watt . 'W%')
						->orWhere('vtext3', 'LIKE', '% ' . $watt . 'W%')
							->orWhere('vtext4', 'LIKE', '% ' . $watt . 'W%');
			});
		}

		if($mA){
			$items->where(function($query) use($mA) {
				$query->where('vtext', 'LIKE',  '% ' . $mA . 'mA%')
					->orWhere('vtext2', 'LIKE', '% ' . $mA . 'mA%')
						->orWhere('vtext3', 'LIKE', '% ' . $mA . 'mA%')
							->orWhere('vtext4', 'LIKE', '% ' . $mA . 'mA%');
			});
		}
		
		if($volt){
			$items->where(function($query) use($volt) {
				$query->where('vtext', 'LIKE',  '% ' . $volt . 'V%')
					->orWhere('vtext2', 'LIKE', '% ' . $volt . 'V%')
						->orWhere('vtext3', 'LIKE', '% ' . $volt . 'V%')
							->orWhere('vtext4', 'LIKE', '% ' . $volt . 'V%');
			});
		}

		if($searchVtext){
			$items->where(function($query) use($searchVtext) {
				$query->where('vtext', 'LIKE', '%' . $searchVtext . '%')
					->orWhere('vtext2', 'LIKE', '%' . $searchVtext . '%')
						->orWhere('vtext3', 'LIKE', '%' . $searchVtext . '%')
							->orWhere('vtext4', 'LIKE', '%' . $searchVtext . '%');
			});
		}


		if($sortBy == "watt"){
			$items->orderBy(DB::raw("CAST(SUBSTRING_INDEX(
			LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
			GREATEST(LOCATE('W ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
			LOCATE('W ', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1) AS UNSIGNED)"), $sortOrder);

		} else if($sortBy == "mA") {
			$items->orderBy(DB::raw("SUBSTRING_INDEX(
			LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
			GREATEST(LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
			LOCATE(BINARY 'mA', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1)"), $sortOrder);
		} else if($sortBy == "volt") {
			$items->orderBy(DB::raw("SUBSTRING_INDEX(
		LEFT(CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4), 
		GREATEST(LOCATE(BINARY 'V', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)),
		LOCATE(BINARY 'V', CONCAT(vtext, ' ', vtext2, ' ', vtext3, ' ', vtext4)))-1), ' ', -1)"), $sortOrder);
		
		} else if ($sortBy == "beholdning"){
			$items->orderBy(DB::raw('sum(l_locbeholdning.beholdning)'),$sortOrder);
		
		} else if ($sortBy) {
			$items = $items->orderBy($sortBy, $sortOrder);
		}


		$count = $items->count();

		$items->take($number)->skip($offset);


		//dd(Item::all()->count()->toSql());
		//dd($items->toSql());
		$instance = $items->get();
			
			
			
			
		return [
			'count' => $count,
			'items' => $instance,
			'totalCount' => Item::Driver()->count(),
		];
	}
	
	
	
	public static function getAll($offset, $number, $search, $sortBy, $sortOrder, $search) {
		$items = Item::take($number)->skip($offset);
			
			
		if ($search) {
			$items->where(function($query) use($search) {
				$query->where('varenr', 'LIKE', '%' . $search);
			});
			
			$count = Item::where(function($query) use($search) {
				$query->where('varenr', 'LIKE', '%' . $search);
			})->count();
		} else {
			$count = Item::count();
		}


		if ($sortBy) {
			$items = $items->orderBy($sortBy, $sortOrder);
		}
			
		
			
		//dd(Item::all()->count()->toSql());
		//dd($items->toSql());
		$instance = $items->get();
			
		return [
			'count' => $count,
			'items' => $instance,
			'totalCount' => Item::count(),
		];
	}

}
