<?php

class PC extends Eloquent {

    protected $table = 'BGInfotable';
	
	protected $primaryKey = 'Time_Stamp';
	
	protected $connection = 'sqlsrv';
	

	public static function getSystem($hostname){
		
		$pcdetail = DB::connection('mysql')->table('Equipment')->select('Manufacture','Model')->where('Name', 'LIKE', $hostname)->first();

		if( count($pcdetail) != 1 ) {

			return "unknown";
		}
		
		return $pcdetail->Manufacture . " " . $pcdetail->Model;
	}

	public static function getAll($offset, $number, $search, $sortBy, $sortOrder, $search) {
			$pcs = PC::take($number)->skip($offset);
			
			
			if ($search) {
				$pcs->where(function($query) use($search) {
					$query->where('User_Name', 'LIKE', '%' . $search . '%')->orWhere('Host_Name', 'LIKE', '%' . $search . '%');
				});
			
				$pcCount = PC::where(function($query) use($search) {
					$query->where('User_Name', 'LIKE', '%' . $search . '%')->orWhere('Host_Name', 'LIKE', '%' . $search . '%');;
				})->count();
			} else {
				$pcCount = PC::all()->count();
			}
			
			if ($sortBy) {
				$pcs = $pcs->orderBy($sortBy, $sortOrder);
			}
			
			
			//dd($pcs->toSql());
			$pcInstance = $pcs->get();
			
			return [
				'count' => $pcCount,
				'pcs' => $pcInstance,
				'totalCount' => PC::all()->count(),
			];
	}

}
