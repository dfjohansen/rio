<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/test/', function()
{
    return 'Hello World';
});

Route::resource('pc', 'PCController');

Route::get('/', array('as' => 'driver1', 'uses' => 'DriverController@index'));

Route::get('/driver', array('as' => 'driver/index', 'uses' => 'DriverController@index'));

Route::get('/driver/test', array('as' => 'driver/test', 'uses' => 'DriverController@test'));

Route::get('/led', array('as' => 'led/index', 'uses' => 'LEDController@index'));

