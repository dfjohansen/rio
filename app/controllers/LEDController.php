<?php

class LEDController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		
		if (Request::ajax()) {
			
			$columns = array(
				0 => 'varenr',
				1 => 'vtext',
				2 => 'watt',
				3 => 'lm',
				4 => 'mA',
				5 => 'K',
				6 => 'CRI',
				7 => 'aktiv',
				8 => 'beholdning'
			);
			
			$sortBy = null;
			if (isset($columns[\Input::get('iSortCol_0')])) {
				$sortBy = $columns[\Input::get('iSortCol_0')];
			}
			
			$items = LED::getAllLED(
				Input::get('iDisplayStart'),
				Input::get('iDisplayLength'),
				Input::get('sSearch'),
				$sortBy,
				Input::get('sSortDir_0', 'asc'),
				Input::get('sSearch', null), 
				Input::get('sSearch_0', null),
				Input::get('sSearch_1', null),
				Input::get('sSearch_2', null),
				Input::get('sSearch_3', null),
				Input::get('sSearch_4', null),
				Input::get('sSearch_5', null),
				Input::get('sSearch_6', null),
				Input::get('sSearch_7', null)
				
			);
			
			
			
			$jsonItems = [];
			
			foreach ($items['items'] as $item) {
				
				$itemdata = [
					link_to('http://192.168.0.204/lis/riegens/lagreg/form.php?varenr='.$item->varenr, $item->varenr, array( 'target' => '_blank') ),
					LED::fullVtext($item->varenr),
					$item->watt,
					$item->lm,
					$item->mA,
					$item->K,
					$item->CRI,
					$item->aktiv,
					(int)LED::beholdning($item->varenr)
				];
				
				
				
				$jsonItems[] = $itemdata;
				
			}
			
			$data = [];
			
			$data = [
				'sEcho' => \Input::get('sEcho'),
				'iTotalRecords' => $items['totalCount'],
				'iTotalDisplayRecords' => $items['count'],
				'aaData' => $jsonItems,
			];
			return \Response::json($data);
		
		} 
		else 
		{
			$this->layout->content =  \View::make('led.index');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

