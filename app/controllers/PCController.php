<?php

class PCController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		if (Request::ajax()) {

			$columns = array(
				0 => 'Time_Stamp',
				1 => 'User_Name',
				2 => 'Host_Name',
				3 => 'OS_Version',
				4 => 'IP_Address',
				5 => 'Network_Speed',
				6 => 'CPU',
				7 => 'Memory',
			);
			
			$sortBy = null;
			if (isset($columns[\Input::get('iSortCol_0')])) {
				$sortBy = $columns[\Input::get('iSortCol_0')];
			}
			
			$pcs = PC::getAll(
				Input::get('iDisplayStart'),
				Input::get('iDisplayLength'),
				Input::get('sSearch'),
				$sortBy,
				Input::get('sSortDir_0', 'asc'),
				Input::get('sSearch', null)
			);
			
			$jsonpc = [];
			
			foreach ($pcs['pcs'] as $pc) {
				
				$pcdata = [
					utf8_encode($pc->Time_Stamp),
					utf8_encode($pc->User_Name),
					utf8_encode($pc->Host_Name),
					utf8_encode($pc->OS_Version),
					utf8_encode($pc->IP_Address),
					utf8_encode($pc->Network_Speed),
					utf8_encode($pc->CPU),
					utf8_encode($pc->Memory),
					utf8_encode(PC::getSystem($pc->Host_Name))
				];
				
				
				$jsonpc[] = $pcdata;
				
			}
			
			
			
			//var_dump($jsonpc);
			
			$data = [];
			
			$data = [
				'sEcho' => \Input::get('sEcho'),
				'iTotalRecords' => $pcs['totalCount'],
				'iTotalDisplayRecords' => $pcs['count'],
				'aaData' => $jsonpc,
			];
			
			//dd($data);
			
			//dd($data);
			return \Response::json($data);
		
		} 
		else 
		{
		$this->layout->content =  \View::make('pc.index');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
