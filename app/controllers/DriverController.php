<?php

class DriverController extends \BaseController {


	public function test()
	{
		dd(Item::Driver()->select('l_artikel.vtext', 'l_artikel.varenr', DB::raw('sum(l_locbeholdning.beholdning) as total'))->join('l_locbeholdning', 'l_artikel.varenr', '=', 'l_locbeholdning.varenr')->groupBy('l_locbeholdning.varenr')->get()->toArray());
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		
		if (Request::ajax()) {
			
			$columns = array(
				0 => 'l_artikel.varenr',
				1 => 'vtext',
				2 => 'watt',
				3 => 'mA',
				4 => 'volt',
				5 => 'aktiv',
				6 => 'beholdning', 
				7 => 'd_lengde',
				8 => 'd_bredde',
				9 => 'd_tykkelse',
				10 => 'hulafst'
				
			);
			
			$sortBy = null;
			if (isset($columns[\Input::get('iSortCol_0')])) {
				$sortBy = $columns[\Input::get('iSortCol_0')];
			}
			
			$items = Item::getAllDriver(
				Input::get('iDisplayStart'),
				Input::get('iDisplayLength'),
				Input::get('sSearch'),
				$sortBy,
				Input::get('sSortDir_0', 'asc'),
				Input::get('sSearch', null), 
				Input::get('sSearch_0', null),
				Input::get('sSearch_1', null),
				Input::get('sSearch_2', null),
				Input::get('sSearch_3', null),
				Input::get('sSearch_4', null), 
				Input::get('sSearch_5', null), 
				Input::get('sSearch_7', null), 
				Input::get('sSearch_8', null),
				Input::get('sSearch_9', null),
				Input::get('sSearch_10', null)  
			);
			
			
			
			$jsonItems = [];
			
			foreach ($items['items'] as $item) {
				
				$itemdata = [
					link_to('http://192.168.0.204/lis/riegens/lagreg/form.php?varenr='.$item->varenr, $item->varenr, array( 'target' => '_blank') ),
					Item::fullVtext($item->varenr),
					$item->watt,
					$item->mA,
					$item->volt,
					$item->aktiv,
					$item->total, 
					$item->d_lengde,
					$item->d_bredde,
					$item->d_tykkelse,
					$item->hulafst
				];
				
				
				
				$jsonItems[] = $itemdata;
				
			}
			
			$data = [];
			
			$data = [
				'sEcho' => \Input::get('sEcho'),
				'iTotalRecords' => $items['totalCount'],
				'iTotalDisplayRecords' => $items['count'],
				'aaData' => $jsonItems,
			];
			return \Response::json($data);
		
		} 
		else 
		{
			$this->layout->content =  \View::make('driver.index');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

