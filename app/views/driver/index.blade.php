@section('title')
	Drivers
@stop

@section('css')
<link href="{{{ Config::get('app.url') }}}/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<style>tfoot {
    display: table-header-group;
}
input[type="text"] {
     width: 100%; 
     box-sizing: border-box;
     -webkit-box-sizing:border-box;
     -moz-box-sizing: border-box;
}
</style>
@stop



@section('javascript-files')
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

@stop

@section('javascript')
<script>
jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
    var _that = this;
 
    if ( iDelay === undefined ) {
        iDelay = 250;
    }
 
    this.each( function ( i ) {
        $.fn.dataTableExt.iApiIndex = i;
        var
            $this = this,
            oTimerId = null,
            sPreviousSearch = null,
            anControl = $( 'input', _that.fnSettings().aanFeatures.f );
 
            anControl.unbind( 'keyup search input' ).bind( 'keyup search input', function() {
            var $$this = $this;
 
            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val();
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter( anControl.val() );
                }, iDelay);
            }
        });
 
        return this;
    } );
    return this;
};
</script>
<script>

        $(document).ready(function() {
                
        $('#mytable').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "iDisplayLength": 500,
						"bAutoWidth": false,
                        "sAjaxSource": "{{{ route('driver/index') }}}",
						"columnDefs": [
						    { "width": "5%", "targets": [5] }
						]
                }).fnSetFilteringDelay(300);
                
                
                                
                $('#mytable tfoot th').each( function () {
                        var title = $('#mytable thead th').eq( $(this).index() ).text();
                        $(this).html( '<input type="text" id="'+title+'" placeholder="" />' );
                } );
				
                
                var stable = $( '#mytable' ).DataTable();
                
                // Apply the filter
                stable.columns().eq( 0 ).each( function ( colIdx ) {
                        $( 'input', stable.column( colIdx ).footer() ).on( 'keyup change', function () {
                                stable
                                .column( colIdx )
                                .search( this.value )
                                .draw();
                        } );
                } );
				
				$('#mytable tfoot th')[6].innerHTML = $('#mytable thead th')[6].innerHTML;
				
				
				$('#Aktiv')[0].value = "J";	
				
                stable
                .column( 5 )
                .search( "J" )
                .draw();
				
				
    }
);
        
        
</script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                Drivers
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="mytable">
                        <thead>
                            <tr>
                                <th>Varenr</th>
								<th>Vare Text</th>
								<th>Watt</th>
								<th>mA</th>
								<th>Volt</th>
								<th>Aktiv</th>
								<th>Beholdning</th>
								<th>Længde</th>
								<th>Bredde</th>
								<th>Højde</th>
								<th>Hulafstand</th>
								
                            </tr>
                        </thead>
						
                        <tfoot>
                            <tr>
                               <th>Varenr</th>
							<th>Vare Text</th>
							<th>Watt</th>
							<th>mA</th>
							<th>Volt</th>
							<th>Aktiv</th>
							<th>Beholdning</th>
							<th>Længde</th>
							<th>Bredde</th>
							<th>Højde</th>
							<th>Hulafstand</th>
                            </tr>
                        </tfoot>
						
						<tbody>
							<tr>
								<td colspan="11" class="dataTables_empty"><i class="fa fa-spinner fa-spin"></i> Loading data from server</td>
							</tr>
						</tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop
