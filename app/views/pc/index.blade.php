@section('title')
	PC
@stop

@section('css')
<link href="{{{ Config::get('app.url') }}}/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
@stop



@section('javascript-files')
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

@stop

@section('javascript')
<script>
    $(document).ready(function() {
        var table = $('#pcer').dataTable({
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": "{{{ route('pc.index') }}}",
			"order": [[ 0, "desc" ]], 
			"iDisplayLength": 60,
			"aoColumnDefs": [
      			{ "bSortable": false, "aTargets": [ 8 ] }
    			],
			
			
		});
		setInterval( function () {
		    table.api().ajax.reload();
		}, 30000 );
		$.fn.dataTableExt.sErrMode = 'throw';
    });
	
	
</script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
		<div class="panel panel-default">
            <div class="panel-heading">
                PC'er
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="pcer">
                        <thead>
                            <tr>
                                <th>Time_Stamp</th>
								<th>User_Name</th>
								<th>Host_Name</th>
								<th>OS_Version</th>
								<th>IP_Address</th>
								<th>Network_Speed</th>
								<th>CPU</th>
								<th>Memory</th>
								<th>System</th>
                            </tr>
                        </thead>
						<tbody>
							<tr>
								<td colspan="8" class="dataTables_empty"><i class="fa fa-spinner fa-spin"></i> Loading data from server</td>
							</tr>
						</tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop
