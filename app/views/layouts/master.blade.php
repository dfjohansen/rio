<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{{ Config::get('app.name') }}} - @yield('title')</title>

    <!-- Core CSS - Include with every page -->
    <link href="{{{ Config::get('app.url') }}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{{ Config::get('app.url') }}}/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Blank -->
	@yield('css')

    <!-- SB Admin CSS - Include with every page -->
    <link href="{{{ Config::get('app.url') }}}/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    @yield('main-content')

    <!-- Core Scripts - Include with every page -->
    <script src="{{{ Config::get('app.url') }}}/js/jquery-1.11.0.js"></script>
    <script src="{{{ Config::get('app.url') }}}/js/bootstrap.min.js"></script>
    <script src="{{{ Config::get('app.url') }}}/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Blank -->
	@yield('javascript-files')

    <!-- SB Admin Scripts - Include with every page -->
    <script src="{{{ Config::get('app.url') }}}/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->
	@yield('javascript')
</body>

</html>
