@extends('layouts.master')

@section('main-content')
    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ Config::get('app.url') }}}">{{{ Config::get('app.name') }}}</a>
            </div>
            <!-- /.navbar-header -->

            
        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{{ Config::get('app.url') }}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
		
                    <li>
                        <a href="{{{ route('driver/index') }}}"><i class="fa fa-users fa-fw"></i> Driver oversigt</a>
                    </li>
		<li>
                        <a href="{{{ route('led/index') }}}"><i class="fa fa-users fa-fw"></i> LED oversigt</a>
                    </li>
							
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('title')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			@yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop
